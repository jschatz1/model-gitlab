from setuptools import setup, find_packages

setup(
    name='model-gitlab',
    version='0.1',
    description='Meltano .m5o models for data fetched using the Gitlab API',
    packages=find_packages(),
    package_data={'models': ['*.m5o']},
    install_requires=[],
)
